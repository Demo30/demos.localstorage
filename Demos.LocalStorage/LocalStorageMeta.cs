﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Demos.DemosHelpers;

namespace Demos.LocalStorage
{
    public static class LocalStorageMeta
    {
        public static Versioning Version
        {
            get
            {
                string versionString = Assembly.GetExecutingAssembly()
                    .GetName()
                    .Version
                    .ToString();
                Versioning versioning = new Versioning();
                versioning.LoadVersion(versionString);
                return versioning;
            }
        }
    }
}
