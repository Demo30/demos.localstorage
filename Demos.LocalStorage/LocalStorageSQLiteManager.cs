﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using System.Data.SQLite;
using Demos.DemosHelpers;
using Demos.DatabaseMigrations;


namespace Demos.LocalStorage
{
    public class LocalStorageSQLiteManager : LocalStorageManager
    {
        public SQLiteConnection GetLocalDatabase(DatabaseMigrationManager manager, TypeOfData typeOfData, string[] containingSubFolders)
        {
            string pathToDatabaseFile = this.GetPathToDatabaseFile(manager, typeOfData, containingSubFolders);
            return new SQLiteConnection($"Data Source={pathToDatabaseFile};Version=3;");
        }

        public void EstablishLocalDatabaseIfNeed(DatabaseMigrationManager manager, TypeOfData typeOfData, string[] containingSubfolders)
        {
            if (manager == null)
            {
                throw new ArgumentNullException();
            }

            bool establishDatabase = false;

            string pathToDatabaseFile = this.GetPathToDatabaseFile(manager, typeOfData, containingSubfolders);

            if (!File.Exists(pathToDatabaseFile))
            {
                establishDatabase = true;
            }

            try
            {
                if (establishDatabase)
                {
                    SQLiteConnection.CreateFile(pathToDatabaseFile);
                }

                using (IDbConnection connection = this.GetLocalDatabase(manager, typeOfData, containingSubfolders))
                {
                    connection.Open();

                    if (establishDatabase)
                    {
                        manager.InitializeMigrationTable(connection);
                    }

                    manager.LoadResources();
                    manager.UpdateMigrations(new MIGRATION_TYPES[] { MIGRATION_TYPES.BASIC_DATA, MIGRATION_TYPES.DUMMY_DATA, MIGRATION_TYPES.STRUCTURE }, connection);
                }
            }
            catch
            {
                File.Delete(pathToDatabaseFile);
                throw;
            }
        }

        private string GetPathToDatabaseFile(DatabaseMigrationManager manager, TypeOfData typeOfData, string[] containingSubFolders)
        {
            string dir = this.GetPathToDirectory(typeOfData, true, containingSubFolders, true);
            string pathToDatabase = String.Concat(dir, "\\", manager.GetDatabaseFileName());
            return pathToDatabase;
        }
    }
}
