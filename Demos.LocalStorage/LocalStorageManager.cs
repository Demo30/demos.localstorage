﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Demos.DemosHelpers;
using System.Data;

namespace Demos.LocalStorage
{
    /// <summary>
    /// Any local data should be saved and loaded using this manager.
    /// By local data, it is understood, any data that used by the application, not present after compilation, stored locally per installation.
    /// </summary>
    public class LocalStorageManager
    {
        private const string _defaultLocalDataDirectoryPath = "LocalAppData";
        private const string _userDataDir = "UserData";
        private const string _appDataDir = "AppData";

        /// <summary>
        /// Saves data into file withing local-data directory structure.
        /// </summary>
        /// <param name="type">File is saved into proper folder depending on type</param>
        /// <param name="data">Actual data in array of bytes to be saved</param>
        /// <param name="filename">Relative name of the file being saved including extension</param>
        /// <param name="relativeFolderStructure">Structure of possible subdirectories within which the file should be saved</param>
        public void SaveData(TypeOfData type, byte[] data, string filename, string[] relativeFolderStructure)
        {
            if (String.IsNullOrEmpty(filename) || data == null || data.Length == 0) { throw new ArgumentNullException(); }

            this.EstablishBasicDirectoryStructureIfNeed();

            string path = this.GetPathToFile(type, filename, relativeFolderStructure, false, true);

            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                fs.Write(data, 0, data.Length);
            }
        }

        public byte[] LoadData(TypeOfData type, string filename, string[] relativeFolderStructure)
        {
            if (String.IsNullOrEmpty(filename)) { throw new ArgumentNullException(); }

            string path = this.GetPathToFile(type, filename, relativeFolderStructure, false, false);

            if (File.Exists(path) == false)
            {
                throw new ArgumentException($"Could not locate any file on path: {path}");
            }

            byte[] data = null;

            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                int size = (int)fs.Length;
                data = new byte[size];
                fs.Read(data, 0, size);
            }

            if (data == null)
            {
                throw new Exception("Internal error, failed to read the file.");
            }

            return data;
        }

        public string GetPathToFile(TypeOfData type, string filename, string[] relativeSubDirStructure, bool relative, bool establish)
        {
            string pathToDir = this.GetPathToDirectory(type, relative, relativeSubDirStructure, establish);

            string pathToFile = String.Concat(pathToDir, "\\", filename);

            return pathToFile;
        }

        public string GetPathToDirectory(TypeOfData type, bool relative, string[] relativeSubDirStructure, bool establish)
        {
            string basicDirPath = this.GetBasicDirPath(type, relative);

            if (establish)
            {
                this.EstablishRelativeStructureIfNeed(type, relativeSubDirStructure);
            }

            if (relativeSubDirStructure == null || relativeSubDirStructure.Length == 0)
            {
                string pathToDir = basicDirPath;
                return pathToDir;
            }
            else
            {
                string pathToDir = String.Concat(basicDirPath, relativeSubDirStructure.ArrayToString("", new Tuple<string, string>("\\", "")));
                return pathToDir;
            }
        }

        public void EstablishBasicDirectoryStructureIfNeed()
        {
            string mainDirFullPath = LocalStorageManager.GetMainLocalDataFolder(false);
            if (Directory.Exists(mainDirFullPath) == false)
            {
                Directory.CreateDirectory(mainDirFullPath);
            }

            string appDataFullPath = this.GetBasicDirPath(TypeOfData.AppData, false);
            if (Directory.Exists(appDataFullPath) == false)
            {
                Directory.CreateDirectory(appDataFullPath);
            }

            string userDataFullPath = this.GetBasicDirPath(TypeOfData.UserData, false);
            if (Directory.Exists(userDataFullPath) == false)
            {
                Directory.CreateDirectory(userDataFullPath);
            }
        }

        private string GetBasicDirPath(TypeOfData type, bool relative)
        {
            string mainDir = LocalStorageManager.GetMainLocalDataFolder(relative);

            switch (type)
            {
                case TypeOfData.UserData: return $@"{mainDir}\{LocalStorageManager._userDataDir}";
                case TypeOfData.AppData: return $@"{mainDir}\{LocalStorageManager._appDataDir}";
                default: throw new NotImplementedException();
            }
        }

        private static string GetMainLocalDataFolder(bool relative)
        {
            if (relative)
            {
                return LocalStorageManager._defaultLocalDataDirectoryPath;
            }
            else
            {
                return $@"{Environment.CurrentDirectory}\{LocalStorageManager._defaultLocalDataDirectoryPath}";
            }
        }

        private void EstablishRelativeStructureIfNeed(TypeOfData type, string[] structureOfSubDirs)
        {
            this.EstablishBasicDirectoryStructureIfNeed();

            if (structureOfSubDirs == null || structureOfSubDirs.Length == 0)
            {
                return;
            }

            string path = this.GetBasicDirPath(type, false);

            for (int i = 0; i < structureOfSubDirs.Length; i++)
            {

                string curSubDirPath = String.Concat(path, '\\', structureOfSubDirs[i]);
                if (Directory.Exists(curSubDirPath) == false)
                {
                    Directory.CreateDirectory(curSubDirPath);
                }

                path = String.Concat(path, '\\', structureOfSubDirs[i]);
            }
        }
    }
}
